<template>
    <div class="home">
        <nav :class="sidebarClass">
            <p class="toggle-button" @click="sidebarOpen = !sidebarOpen">≡</p>
            <div v-if="sidebarOpen">
                <h4 class="sidebar-title">CSV logs to parse</h4>
                <csv-reader @load="source = $event"/>

                <h4 class="sidebar-title">Display SVG graph</h4>
                <input type="checkbox" id="display-graph" v-model="displayGraph"/>
                <label for="display-graph">(Disable this for stats only)</label>

                <h4 class="sidebar-title">Operations types to display</h4>
                <multiselect
                    v-model="selectedOpTypes"
                    :options="availableOpTypes"
                    :multiple="true"
                    :close-on-select="false"
                    placeholder="Operations to show"
                    label="name"
                    track-by="name"
                />

                <h4 class="sidebar-title">Time factor for display <em>(svg_size/second)</em></h4>
                <input type="number" v-model.lazy="timeFactor" :step="1" :min="0" />
                <vue-slider
                    v-model="timeFactor"
                    :min="0"
                    :max="2 * timeFactor"
                    :interval="1"
                    :lazy="true"
                />

                <h4 class="sidebar-title">Max number of operations to show</h4>
                <input type="number" v-model.lazy="maxOperationNumber" :step="1" :min="0" />
                <vue-slider
                    v-model="maxOperationNumber"
                    :min="0"
                    :max="2 * maxOperationNumber"
                    :interval="1"
                    :lazy="true"
                />

                <div v-if="source.length">
                    <h4 class="sidebar-title">Time interval considered <em>(second)</em></h4>
                    <input
                        type="number"
                        v-model="timeBoundaries[0]"
                        :step="1e-9"
                    />
                    →
                    <input
                        type="number"
                        v-model="timeBoundaries[1]"
                        :step="1e-9"
                    />
                    <vue-slider
                        v-if="!hideTimeSlider"
                        v-model="timeBoundaries"
                        :min="minTime"
                        :max="maxTime"
                        :interval="1e-9"
                        :lazy="true"
                    />
                </div>
                <button @click="downloadSvg" 
                        class="colored-border-button" 
                        style="margin-top: 1rem; margin-bottom: 2rem;">
                    Download SVG
                </button>

                <div v-if="source.length">
                    <h4 class="sidebar-title">Stats</h4>
                    <ul>
                        <li v-for="stat in stats" :key="`op_${stat.op}`">
                            {{ stat.count }} <strong>{{ stat.op.name }}</strong> operations have:
                            <ul>
                                <li>An average time of {{ stat.avg.toExponential(3) }}s</li>
                                <li>A min time of {{ stat.min.toExponential(3) }}s</li>
                                <li>A max time of {{ stat.max.toExponential(3) }}s</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main :class="mainClass">
            <div v-if="!source.length" class="default-text">
                <h1>Start by selecting logs in the sidebar</h1>
                <p>
                    You can select several files at once, but make sure not to select too many, the 
                    page might get slow if there are more than a few tens of thousands of lines to parse.
                    Also, everything happens in your browser, your logs are never sent to any server.
                </p>
            </div>
            <div v-else-if="!displayGraph" class="default-text">
                <h1>Stats only mode</h1>
                <p>
                    Toggle the checkbox in the sidebar to generate the SVG graph.
                </p>
            </div>
            <sequence-diagram
                v-else
                :source="filteredSource"
                ref="diagram"
                :time-factor="timeFactor"
                :max-operation-number="maxOperationNumber"
            />
        </main>
    </div>
</template>

<script lang="ts">
import { Component, Vue, Watch } from "vue-property-decorator";
import {
    OperationRow,
    LogFile,
    minTime,
    maxTime
} from "@/util/s4bxi";
import Multiselect from "vue-multiselect";
import { OperationTextType, allOperationTypes } from "@/util/svg";
import VueSlider from "vue-slider-component";

const SequenceDiagram = () => import("@/components/SequenceDiagram.vue");
const CsvReader = () => import("@/components/CsvReader.vue");

@Component({ components: { SequenceDiagram, CsvReader, Multiselect, VueSlider } })
export default class App extends Vue {
    private source: OperationRow[] = [];
    private filteredSource: OperationRow[] = [];

    private selectedFiles: LogFile[] = [];
    private availableFiles: LogFile[] = [];

    private selectedOpTypes: OperationTextType[] = [];
    private availableOpTypes: OperationTextType[] = [];

    private sidebarOpen: boolean = true;

    private timeBoundaries: number[] = [0, 0];

    private timeFactor: number = 5e6;

    private maxOperationNumber: number = 100;

    private hideTimeSlider: boolean = false;

    private displayGraph: boolean = true;

    get sidebarClass(): string[] {
        return this.sidebarOpen ? ["sidebar", "sidebar-open"] : ["sidebar"];
    }

    get mainClass(): string[] {
        return this.sidebarOpen
            ? ["main-area", "main-sidebar-open"]
            : ["main-area"];
    }

    get minTime(): number {
        return minTime(this.source);
    }

    get maxTime(): number {
        return maxTime(this.source);
    }

    get stats(): Array<Object> {
        return this.selectedOpTypes.map((op) => {
            const rows = this.filteredSource.slice(0, this.maxOperationNumber).filter((row) => row.operation_type == op.id);
            return {
                op: op,
                count: rows.length,
                avg: rows.reduce((acc, {start_time, end_time}) => acc + (end_time - start_time) / rows.length, 0),
                min: rows.reduce((acc, {start_time, end_time}) => (end_time - start_time) < acc ? (end_time - start_time) : acc, 9999),
                max: rows.reduce((acc, {start_time, end_time}) => (end_time - start_time) > acc ? (end_time - start_time) : acc, 0)
            };
        }).filter(({count}) => count);
    }

    @Watch("source")
    onSourceUpdate() {
        // This weird trick ensures that we never have incoherent values, in particular
        // if we don't use it we frequently have errors because the slider will complain
        // that the value (timeBoundaries) is outside the allowed range 
        // ([minTime; maxTime]), which happens because minTime and maxTime get updated
        // before timeBoundaries, so hideTimeSlider fixes that
        this.hideTimeSlider = true;
        this.$nextTick(() => {
            this.timeBoundaries = [
                +this.minTime.toFixed(9), // Round to ns, to avoid weird roundig bugs
                +(this.maxTime > this.minTime + 1e-3
                    ? (this.minTime + 1e-3)
                    : this.maxTime
                ).toFixed(9)
            ];
            this.filterSource();
            this.hideTimeSlider = false;
        });
    }

    @Watch("selectedOpTypes")
    onOpTypeSelect() {
        this.filterSource();
    }

    @Watch("timeBoundaries")
    onTimeBoundaryChange() {
        this.filterSource();
    }

    filterSource() {
        this.filteredSource = this.source.filter(
            ({ operation_type, start_time, end_time }) =>
                this.selectedOpTypes.find(({ id }) => id == operation_type) &&
                start_time >= this.timeBoundaries[0] &&
                end_time <= this.timeBoundaries[1]
        );
    }

    downloadSvg() {
        const svgData = (<Vue>this.$refs.diagram).$el.outerHTML;
        const svgBlob = new Blob([svgData], {
            type: "image/svg+xml;charset=utf-8"
        });
        const svgUrl = URL.createObjectURL(svgBlob);
        const downloadLink = document.createElement("a");
        downloadLink.href = svgUrl;
        downloadLink.download = "diagram.svg";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }

    mounted() {
        this.availableOpTypes = allOperationTypes();
        this.selectedOpTypes = allOperationTypes();

        fetch("/csvIndex.json")
            .then(res => res.json())
            .then(data => (this.availableFiles = data));
    }
}
</script>

<style lang="scss">
@import "~vue-multiselect/dist/vue-multiselect.min.css";
@import "~vue-slider-component/theme/default.css";
@import "~typeface-roboto/index.css";

html, body {
    margin: 0;
    padding: 0;
    max-width:100vw;
    min-height: 100vh;
    font-family: Roboto, Calibri, sans-serif;
    --network-color: #0064a2;
    --simulator-color: #00a1c7;
}

.home {
    --closed-sidebar-width: 60px;
    max-width: 100%;
}

.sidebar {
    color: white;
    position: fixed;
    bottom: 0;
    top: 0;
    left: 0;
    width: var(--closed-sidebar-width);
    background-color: #2f363f;
    padding: 5px;
    padding-top: 0 !important;
    transition: all 0.7s;
    text-align: center;


    .toggle-button {
        opacity: 0.5;
        margin-top: 0;
        margin-bottom: 10px;
        font-size: 50px;
        cursor: pointer;
    }

    .toggle-button:hover {
        opacity: 0.8;
    }
}

.sidebar.sidebar-open {
    padding: 1rem;
    width: 25%;
    overflow-y: auto;

    .colored-border-button {
        display: inline-block;
        font-size: 1rem;
        border-radius: 3px;
        background-color: transparent;
        color: white;
        border: 2px solid var(--simulator-color);
        transition-duration: 0.4s;
        padding: 0.5rem 1rem;
        cursor: pointer;
    }

    .colored-border-button:hover {
        background-color: var(--simulator-color);
    }

    h4.sidebar-title {
        font-weight: 300;
    }

    input[type="number"] {
        width: 150px;
        margin: 5px;
        border: 0;
        padding: 7px;
        border-radius: 5px;
        background-color: #ffffff33;
        color: white;
    }

    .multiselect__tag {  // Override vue-multiselect's CSS
        background: var(--network-color);

        .multiselect__tag-icon {
            &::after {
                content: "\D7";
                color: var(--simulator-color);
                font-size: 14px;
            }
            &:focus, &:hover {
                background: var(--simulator-color);
                &::after{
                    color: white;
                }
            }
        }
    }

    .vue-slider-process {  // Override vue-slider's CSS
        background-color: var(--simulator-color);
    }

    ul {
        text-align: left;
        margin-bottom: 1rem;
    }
}

.main-area {
    transition: all 0.7s;
    margin-left: var(--closed-sidebar-width);
}

.main-area.main-sidebar-open {
    margin-left: 25vw;
}

.default-text {
    text-align: center; 
    max-width: 75%;
    margin: auto;

    h1 {
        margin-top: 5rem; 
        font-weight: 100;
        font-size: 3rem;
    }

    p {
        font-size: 1.5rem;
    }
}
</style>
